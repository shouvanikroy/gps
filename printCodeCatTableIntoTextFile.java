package seleniumwebdriver.Assignments;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class printCodeCatTableIntoTextFile {

	static By locatorEmailField = By.xpath("//input[@id='txtName']");
	static By locatorPasswordField = By.xpath("//input[@id='txtPassword']");
	static By locatorLogin = By.xpath("//input[@id='btnLogin']");
	static By locatorCodecat = By.xpath("//a[@title='CodeCat Feedback List']");

	public static void main(String[] args) throws IOException {

		PrintWriter code = new PrintWriter("code.txt");

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\mindfire\\Desktop\\java\\seleniumtraining\\crfiles\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.get("http://www.ourgoalplan.com/Login.aspx");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		// enter email id
		driver.findElement(locatorEmailField).sendKeys("ayusheed");

		// enter password
		driver.findElement(locatorPasswordField).sendKeys("ayushee@mfs2015");

		// click the login button
		driver.findElement(locatorLogin).click();

		// click the codecat feedback
		driver.findElement(locatorCodecat).click();

		// locating the table //table[@id='gridCodeReviewFeedback']
		WebElement table = driver.findElement(By.xpath("//td[@id='tbgridCodeReviewFeedback']"));

		System.out.printf("%-30s %-30s %-25s %20s\n", "Members", "Review By", "Status", "Quality");

		String Members_xpath1 = "//*[@id=\"gridCodeReviewFeedback\"]/tbody//tr[";
		String Members_xpath2 = "]//td[2]";
		String ReviewBy_xpath1 = "//table[@border='1']//table//tbody//tr[";
		String ReviewBy_xpath2 = "]//td[5]";
		String Status_xpath1 = "//table[@border='1']//table//tbody//tr[";
		String Status_xpath2 = "]//td[7]";
		String Quality_xpath1 = "//img[@id='gridCodeReviewFeedback_ctl";
		String Quality_xpath2 = "_imgScore']";
		String src1 = "http://www.ourgoalplan.com/Images/1pxW.jpg";
		String src2 = "http://www.ourgoalplan.com/Images/2.gif";
		String src3 = "http://www.ourgoalplan.com/Images/3.gif";
		String src4 = "http://www.ourgoalplan.com/Images/4.gif";


		List<WebElement> rows = table.findElements(By.xpath("//*[@id=\"gridCodeReviewFeedback\"]/tbody/tr"));

		String formatStr = "%30s %30s %-25s";

		for (int id = 2; id <= 51; id++) {

			// System.out.println(id);
			// int count = id + 100;
			String Members = driver.findElement(By.xpath(Members_xpath1 + id + Members_xpath2)).getText();
			System.out.printf("%-30s", Members);
			// System.out.printf(Quality_xpath1+count+Quality_xpath2);

			String ReviewBy = driver.findElement(By.xpath(ReviewBy_xpath1 + id + ReviewBy_xpath2)).getText();
			System.out.printf("%-30s", ReviewBy);

			String Status = driver.findElement(By.xpath(Status_xpath1 + id + Status_xpath2)).getText(); 
			System.out.printf("%-25s", Status);
          
			
			if (id < 10) {
				String Quality = driver.findElement(By.xpath(Quality_xpath1 + 0 + id + Quality_xpath2))
						.getAttribute("src");
				code.write(String.format(formatStr, Members,ReviewBy,Status));
				if(Quality.equals(src1)) {
					System.out.printf("%20s", "NULL");
					//code.write(String.format(formatStr, Members,ReviewBy,Status));
				    code.write(String.format("%20s%n", "NULL"));}
				else if(Quality.equals(src2)) {
					System.out.printf("%20s%n", "GOOD");
					//code.write(String.format(formatStr, Members,ReviewBy,Status));
				    code.write(String.format("%20s%n", "GOOD"));}
				else if(Quality.equals(src3)) {
					System.out.printf("%20s", "AVERAGE");
					//code.write(String.format(formatStr, Members,ReviewBy,Status));
				    code.write(String.format("%20s%n", "AVERAGE"));}
				else {
					System.out.printf("%20s", "BAD");
				    //code.write(String.format(formatStr, Members,ReviewBy,Status));
			        code.write(String.format("%20s%n", "BAD"));}
				//System.out.printf("%20s", Quality);
				
				System.out.println("\n");
			}

			else {

				String Quality = driver.findElement(By.xpath(Quality_xpath1 + id + Quality_xpath2)).getAttribute("src");
				code.write(String.format(formatStr, Members,ReviewBy,Status));
				if(Quality.equals(src1)) {
					System.out.printf("%20s", "NULL");
					//code.write(String.format(formatStr, Members,ReviewBy,Status));
				    code.write(String.format("%20s%n", "NULL"));}
				else if(Quality.equals(src2)) {
					System.out.printf("%20s", "GOOD");
					//code.write(String.format(formatStr, Members,ReviewBy,Status));
				    code.write(String.format("%20s%n", "GOOD"));}
				else if(Quality.equals(src3)) {
					System.out.printf("%20s", "AVERAGE");
					//code.write(String.format(formatStr, Members,ReviewBy,Status));
				    code.write(String.format("%20s%n", "AVERAGE"));}
				else {
					System.out.printf("%20s", "BAD");
					//code.write(String.format(formatStr, Members,ReviewBy,Status));
				    code.write(String.format("%20s%n", "BAD"));}
				//System.out.printf("%20s", Quality);
				System.out.println("\n");
			}
		}

		code.close();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		// close Chrome
		driver.close();

	}

}
//MASTER